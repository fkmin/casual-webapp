angular.module('webapp').factory('frontendService', [
   'constants',
   'backendService',
   '$sce',
   function (constants, backendService, $sce)
   {

      let domain, service, gateway, transaction;

      return {
         updateStates: updateStates,
         getDomain: getDomain,
         getService: getService,
         getExecutables: getExecutables,
         getServerByAlias: getServerByAlias,
         getGroupByAlias: getGroupByAlias,
         getAnchor: getAnchor,
         getGroupMembers: getGroupMembers,
         scale: scale,
         getServiceByName: getServiceByName
      };

      async function updateStates()
      {
         await update();
         //console.log(await getDomain());

      }

      async function update()
      {

         let domainP = backendService.getDomainState().then(function (response)
         {
            return ({
               name: "domain",
               value: response
            });
         });

         let serviceP = backendService.getServiceState().then(function (response)
         {
            return ({
               name: "service",
               value: response
            });
         });
         let transactionP = backendService.getTransactionState().then(function (response)
         {
            return ({
               name: "transaction",
               value: response
            });
         });
         let gatewayP = backendService.getGatewayState().then(function (response)
         {
            return ({
               name: "gateway",
               value: response
            });
         });

         return Promise.all([domainP, serviceP, transactionP, gatewayP]).then(function (result)
         {

            for (res in result)
            {
               setValue(result[res]);
            }

            // console.log(result);
         })
      };

      function setValue(result)
      {

         switch (result.name)
         {
            case 'domain':
               domain = result.value;
               break;
            case 'service':
               service = result.value;
               break;
            case 'transaction':
               transaction = result.value;
               break;
            case 'gateway':
               gateway = result.value;
               break;
         }


      }

      function errorHandle(p)
      {
         return p.catch(err =>
         {
            console.log("hello")
         });
      }

      async function getDomain()
      {

         return backendService.getDomainState().then(function (response)
         {
            return response.data.result;
         });

      }

      async function getService()
      {
         return backendService.getServiceState().then(function (response)
         {
            return response.data.result;
         });

      }

      async function getServerByAlias(alias)
      {
         let domain = await getDomain();
         let servers = domain.servers;
         let executables = domain.executables;

         for (let i = 0; i < servers.length; i++)
         {
            if (servers[i].alias === alias)
            {
               return servers[i];
            }
         }

         for (let i = 0; i < executables.length; i++)
         {
            if (executables[i].alias === alias)
            {
               return executables[i];
            }
         }
      }

      async function getGroupByAlias(alias)
      {
         let domain = await getDomain();
         let groups = domain.groups;

         for (let i = 0; i < groups.length; i++)
         {
            if (groups[i].name === alias)
            {
               return groups[i];
            }
         }
      }

      async function getGroupMembers(groupId)
      {

         let domain = await getDomain();

         let servers = domain.servers;
         let members = [];

         for (let i = 0; i < servers.length; i++)
         {
            for (let j = 0; j < servers[i].memberships.length; j++)
            {
               if (servers[i].memberships[j] === groupId)
                  members.push(servers[i]);
            }
         }
         return members;
      }

      async function getServiceByName(name)
      {
         let services = await getService();
         for(let i = 0; i < services.services.length; i++)
         {
            if (services.services[i].name === name)
            {
               return services.services[i];
            }
         }
      }

      function getExecutables(pid, callback)
      {
         backendService.getDomainState().then(function (executables)
         {
            let executablesArr = [];

            for (let i = 0; i < executables.length; i++)
            {
               if (executables[i].id === pid)
               {
                  executablesArr.push(executables[i]['alias']);
               }
            }
            callback(executablesArr);
         });

      }

      function scale(alias, instances)
      {

         backendService.scale(alias, instances).then(function(response)
         {

            location.reload(true);
         });

      };
      function getAnchor(url, paramName, param, text)
      {

         let html = `<a class="name-link" ui-sref='${url}({${paramName}: "${param}" })'>${text}</a>`;

         return $sce.trustAsHtml(html);
      };


   }]);