angular.module('webapp').factory('backendService', [
   '$http',
   'constants',
   function ($http, constants)
   {

      return {
         updateStates: updateStates,
         getDomainState: getDomainState,
         getServiceState: getServiceState,
         getTransactionState: getTransactionState,
         getGatewayState: getGatewayState,
         scale: scale
      };

      function updateStates()
      {

      }

      function getDomainState()
      {
         return $http.post(constants.domainURI, {});
      }

      function getServiceState()
      {
         return $http.post(constants.serviceURI, {});
      }

      function getTransactionState()
      {
         return $http.post(constants.transactionURI, {});
      }

      function getGatewayState()
      {
         return $http.post(constants.gatewayURI, {});
      }

      function scale(alias, instan)
      {
         let scaleObj = {
            instances: [
               {
                  alias: alias,
                  instances: instan
               }
            ]
         };

         return $http.post(constants.scaleURI, scaleObj)
      }

   }]);
