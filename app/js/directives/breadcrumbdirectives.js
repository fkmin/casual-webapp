angular.module('webapp').directive('serveBread', ['$compile', function ()
{
   return {
      template: `
          <div class="breadcrumbs">
              <ul>
                  <li ng-repeat="bread in breadcrumbs">
                      <div ng-class="{bread, first : $first, last: $last}" ng-bind-html="addBreadAnchor(bread, $last);"
                           init-bind></div>
                  </li>
              </ul>
          </div>`
   }
}]);
