app.directive('dynLink', function ()
{
   return {
      restrict: 'E',
      replace: true,
      transclude: true,
      scope: {
         url: '@'
      },
      template: `<a ui-sref='instance({ exid:  "{{url}}" })' ui-sref-opts="{inherit:false}"></a>`
      //template: '<a ui-sref="instance({ param: {{url}} })" ui-sref-opts="{inherit:false}" ng-transclude></a>'
   }
});

app.directive('initBind', ['$compile', function ($compile)
{
   return {
      restrict: 'A',
      link: function (scope, element, attr)
      {
         attr.$observe('ngBindHtml', function ()
         {
            if (attr.ngBindHtml)
            {
               $compile(element[0].children)(scope);
            }
         })
      }
   };
}]);