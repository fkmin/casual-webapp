angular.module('webapp').controller("webCtrl", [
   '$transitions',
   '$scope',
   'frontendService',
   'constants',
   '$sce',
   '$compile',
   function ($transitions, $scope, frontendService, constants, $sce, $compile)
   {
      $scope.urlconfig = {
         headers: {
            'Content-Type': 'application/json'
         }
      };

      $scope.version = constants.WEB_VERSION;
      $scope.message = "Hej Casual";

      $scope.getBread = function (url)
      {
         let crumbs = url.split('/');
         console.log(crumbs);
         return crumbs.slice(1);
      };
      $transitions.onSuccess({}, async function (event, param)
      {
         $scope.activeState = event.to().name;
         $scope.breadcrumbs = event.router.urlRouter.location.split('/').slice(1);

      });

      $scope.getExecutables = function (pid)
      {

         let executables = $scope.domain.data.result.executables;

         let executablesArr = [];

         for (let i = 0; i < executables.length; i++)
         {
            if (executables[i].id === pid)
            {
               executablesArr.push(executables[i]['alias']);
            }
         }
         return executablesArr;
      };


      let init = async function ()
      {
         await frontendService.updateStates();
      };

      $scope.addBreadAnchor = function (state, last)
      {
         if(last)
            return `<span class="bread-current">${state}</span>`;

         let html = `<a class="name-link" ui-sref='${state}'>${state}</a>`;

         return $sce.trustAsHtml(html);
      };

      $scope.testfunc = function()
      {
        return "hejsan"
      }

      //init();
   }]);