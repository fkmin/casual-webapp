angular.module('webapp').controller('instanceCtrl', [
   '$sce',
   '$scope',
   '$stateParams',
   'frontendService',
   'instancesInfo',
   function ($sce, $scope, $stateParams, frontendService, instancesInfo)
   {

      $scope.exid = $stateParams.exid;

      $scope.domain = instancesInfo.domain;
      $scope.instances = instancesInfo.services.instances;

      $scope.addDom = function (pid)
      {
         return getExecutable(pid);

      };

      let getExecutable = function (pid)
      {

         let executableID = "";
         let executableName = "";

         let servers = $scope.domain.servers;
         servers.concat($scope.domain.executables);
        // console.log(executables);
         for (let i = 0; i < servers.length; i++)
         {
            let instances = servers[i].instances;
            if (instances.length !== 0)
            {
               for (let j = 0; j < instances.length; j++)
               {
                 //if(j%10 === 0) console.log(instances[j], pid);
                  if (instances[j].handle.pid === pid || instances[j].handle === pid)
                  {
                     executableID = servers[i].id;
                     executableName = servers[i].alias;
                  }
               }
            }

         }
         return frontendService.getAnchor('server', 'alias', executableName, executableName);
         /*
         if (executableID === "")
         {
            return ('<div>None</div>');
         } else
         {
            let html = `<a ui-sref='instance({ exid: "${executableID}" })'>${executableName}</a>`;
            return ($sce.trustAsHtml(html));
         }
         */
      };

      /*

      $scope.addAnchor(url, paramName, param)
      {
         return frontendService.getAnchor(url, paramName, param);
      }

*/
   }
]);