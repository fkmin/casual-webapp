angular.module('webapp').controller('groupCtrl', ['$scope', 'frontendService', 'groupInfo', function ($scope, frontendService, groupInfo)
{
   $scope.group = groupInfo;

   $scope.selectedGroup = groupInfo.group.name;
   $scope.members = groupInfo.members;

   $scope.all = groupInfo.all;

   //console.log(groupInfo);


   $scope.dependcyNames = groupInfo.dependcyNames;

   $scope.addAnchor = function (url, paramName, param, text)
   {


      return frontendService.getAnchor(url, paramName, param, text);


   };


}]);