angular.module('webapp').controller('domainCtrl', [
   '$scope',
   '$stateParams',
   'frontendService',
   'domain',
   function ($scope, $stateParams, frontendService, domain)
   {

      $scope.serverAlias = $stateParams.alias;
      $scope.domain = domain;


      $scope.getInstances = function (servers, type)
      {
         let instances = [];
         for (s in servers)
         {

            let server = type === 0 ? servers[s].handle.pid : servers[s].handle;

            instances.push(server);
         }
         return instances.length;
      };

      let getServerInstances = function (alias)
      {
         let servers = $scope.domain.servers;

         for (let i = 0; i < servers.length; i++)
         {
            if (servers[i].alias === alias)
            {
               return servers[i].instances;
            }
         }
      };


      $scope.getMembers = function (groupid)
      {
         let members = [];
         let servers = $scope.domain.servers;
         servers.concat($scope.domain.executables);

         for (let s = 0; s < servers.length; s++)
         {
            if (isMember(servers[s].memberships, groupid))
            {
               members.push(servers[s].alias);
            }
         }
         return members;
      };

      $scope.getGroupNames = function (memberships)
      {
         let groupNames = [];
         let groups = $scope.domain.groups;

         for (let i = 0; i < memberships.length; i++)
         {
            for (let j = 0; j < groups.length; j++)
            {
               if (memberships[i] === groups[j].id)
               {
                  groupNames.push(groups[j].name);
               }
            }
         }
         return groupNames;
      };

      $scope.getSome = function (stuff)
      {
         return ["one", "two"];
      }

      let isMember = function (serv, id)
      {
         for (let i = 0; i < serv.length; i++)
         {
            if (serv[i] === id)
            {
               return true;
            }
         }
         return false;
      };

      $scope.addAnchor = function (url, paramName, param, text)
      {


         return frontendService.getAnchor(url, paramName, param, text);


      };




   }]);