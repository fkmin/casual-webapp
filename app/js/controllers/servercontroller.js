angular.module('webapp').controller('serverCtrl', [
   '$scope',
   'serverInfo',
   'frontendService',
   function ($scope, serverInfo, frontendService)
   {
      $scope.server = serverInfo;
      $scope.serverName = serverInfo.selected.alias;
      $scope.serverInstances = serverInfo.selected.instances;


      $scope.serverPID = [];
      for (let i = 0; i < $scope.serverInstances.length; i++)
      {
         $scope.serverPID.push($scope.serverInstances[i].handle.pid);
      }


      let getServiceNames = function ()
      {
         let services = serverInfo.services;
         let selected = $scope.serverPID;
         let foundServices = [];
         serviceLoop:
             for (let i = 0; i < services.length; i++)
             {
                for (let j = 0; j < services[i].instances.length; j++)
                {
                   if (selected.includes(services[i].instances[j].pid))
                   {
                      foundServices.push(services[i].name);
                      continue serviceLoop;
                   }
                }
             }
         return foundServices;
      };

      $scope.scale = function (instances)
      {
         frontendService.scale( serverInfo.selected.alias,instances);
      };


      $scope.services = getServiceNames();

      $scope.addAnchor = function (url, paramName, param, text)
      {
         return frontendService.getAnchor(url, paramName, param, text);
      };

      $scope.scaleServer = function (scale)
      {
         if (scale > 0)
         {
            console.log("scale up");
         } else
         {
            console.log("scale down");
         }
      }
   }])
;