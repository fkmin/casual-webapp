angular.module('webapp').config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider)
{

   let casual = '/casual';

   let domain = ['frontendService', function (frontendService)
   {
      return frontendService.getDomain().then(function (domain)
      {
         return domain;
      });
   }];

   let services = ['frontendService', function (frontendService)
   {
      return frontendService.getService().then(function (services)
      {
         return services;
      });
   }];

   let instancesInfo = ['frontendService', function (frontendService)
   {

      let info = {};

      return frontendService.getService()
          .then(function (services)
          {
             info.services = services;
             return frontendService.getDomain();
          })
          .then(function (domain)
          {
             info.domain = domain;
             return info;
          })

   }];

   let buildInfoObject = function (info, domain)
   {

      info.all = domain.groups;
      let all = domain.groups;
      let deps = info.group.dependencies;
      let dependencyNames = [];
      for (let i = 0; i < deps.length; i++)
      {

         for (let j = 0; j < all.length; j++)
         {
            if (deps[i] === all[j].id)
            {
               dependencyNames.push(domain.groups[i].name);
            }
         }
      }

      info.dependcyNames = dependencyNames;
      return info;
   };

   let buildServerObject = function (server, result)
   {
      server.services = [];
      for (let i = 0; i < result.services.length; i++)
      {
         let seq = result.services[i].instances.sequential;
         let con = result.services[i].instances.concurrent;
         let instancesArr = seq.concat(con);
         let serviceNameInstances = {
            name: result.services[i].name,
            instances: instancesArr
         };
         server.services.push(serviceNameInstances);
      }

      return server;
   };
   let buildServiceObject = function (servers, serviceObj)
   {
      serviceObj.server = "";

      let pids = getServicePIDs(serviceObj.service.instances);


      for (let i = 0; i < servers.length; i++)
      {
         if (getServerPIDs(servers[i]).includes(pids[0].pid))
         {
            serviceObj.server = servers[i];
            break;
         }
      }
      return serviceObj;

   };

   let getServicePIDs = function (instances)
   {
      let seq = instances.sequential;
      let con = instances.concurrent;
      return seq.concat(con);

   };
   let getServerPIDs = function (server)
   {
      let pids = [];
      let seq = server.instances;

      for (let i = 0; i < seq.length; i++)
      {
         pids.push(seq[i].handle.pid);
      }
      return pids;
   };

   let servicesState = {
      name: 'services',
      url: `${casual}/services`,
      templateUrl: 'views/services.html',
      controller: 'serviceCtrl',

      resolve: {
         serviceInfo: services,
         domain: domain,
         selected: [function ()
         {
            return null
         }]
      }
   };

   let serviceState = {
      name: 'service',
      url: `${casual}/services/:service`,
      templateUrl: 'views/service.html',
      controller: 'serviceCtrl',

      resolve: {
         serviceInfo: ['$stateParams', 'frontendService', function ($stateParams, frontendService)
         {

            let serviceObj = {};
            return frontendService.getServiceByName($stateParams.service)
                .then(function (service)
                {
                   serviceObj.service = service;

                   return service;
                }).then(function ()
                {
                   return frontendService.getDomain().then(function (domain)
                   {
                      return buildServiceObject(domain.servers, serviceObj);
                   })
                });
         }],
         domain: domain,
         selected: ['$stateParams', function ($stateParams)
         {
            return $stateParams.service;
         }]
      }
   };
   let serversState = {
      name: 'servers',
      url: `${casual}/servers`,
      templateUrl: 'views/servers.html',
      controller: 'domainCtrl',
      resolve: {
         domain: domain
      }
   };
   let serverState = {
      name: 'server',
      url: `${casual}/servers/:server`,
      templateUrl: 'views/server.html',
      controller: 'serverCtrl',
      resolve: {
         serverInfo: ['$stateParams', 'frontendService', function ($stateParams, frontendService)
         {

            let server = {};
            return frontendService.getServerByAlias($stateParams.server)
                .then(function (result)
                {
                   server.selected = result;
                   return result;
                }).then(function ()
                {
                   return frontendService.getService().then(function (services)
                   {
                      return buildServerObject(server, services);
                   })
                });
         }]
      }
   };

   let groupState = {
      name: 'group',
      url: `${casual}/groups/:group`,
      templateUrl: 'views/group.html',
      controller: 'groupCtrl',
      resolve: {
         groupInfo: ['$stateParams', 'frontendService', function ($stateParams, frontendService)
         {
            let info = {};
            return frontendService.getGroupByAlias($stateParams.group).then(function (result)
            {
               info.group = result;
               return result;
            }).then(function (result)
            {
               return frontendService.getGroupMembers(result.id).then(function (members)
               {
                  info.members = members;
                  return info;
               })
            }).then(function (info)
            {
               return frontendService.getDomain().then(function (domain)
               {
                  return buildInfoObject(info, domain);
               })
            });
         }],
      }

   };
   let executableAllState = {
      name: 'executableAll',
      url: '/executableAll',
      templateUrl: 'views/executableAll.html',
      controller: 'domainCtrl'
   };
   let instanceAllState = {
      name: 'instanceAll',
      url: '/instanceAll',
      templateUrl: 'views/instanceAll.html',
      controller: 'instanceCtrl',
      resolve: {
         instancesInfo: instancesInfo
      }
   };
   let instanceState = {
      name: 'instance',
      url: '/instance?exid',
      controller: 'instanceCtrl',
      templateUrl: 'views/instance.html',
      resolve: {
         instancesInfo: instancesInfo
      }
   };


   let transactionAllState = {
      name: 'transactionAll',
      url: '/transactionAll',
      templateUrl: 'views/transactionAll.html'
   };
   let gatewayAllState = {
      name: 'gatewayAll',
      url: '/gatewayAll',
      templateUrl: 'views/gatewayAll.html'
   };
   let groupsState = {
      name: 'groups',
      url: `${casual}/groups`,
      templateUrl: 'views/groups.html',
      controller: 'domainCtrl',
      resolve: {
         domain: domain
      }
   };


   let homeState = {
      name: 'casual',
      url: `${casual}`,
      templateUrl: 'views/casual.html',

   };


   $stateProvider.state(servicesState);
   $stateProvider.state(serviceState);
   $stateProvider.state(serversState);
   $stateProvider.state(serverState);
   $stateProvider.state(transactionAllState);
   $stateProvider.state(gatewayAllState);
   $stateProvider.state(groupsState);
   $stateProvider.state(groupState);
   $stateProvider.state(executableAllState);
   $stateProvider.state(instanceAllState);
   $stateProvider.state(instanceState);
   $stateProvider.state(homeState);


   $urlRouterProvider.otherwise('/casual');


}])
;