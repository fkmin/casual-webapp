let app = angular.module('webapp', ['ui.router', 'ui.bootstrap', 'ngAnimate', 'ngSanitize']);


app.config(['$httpProvider', function ($httpProvider)
{
   $httpProvider.defaults.headers.post['Content-Type'] = 'application/json';
}]);

app.constant('constants', {
   serviceURI: '/casual/.casual/service/state',
   gatewayURI: '/casual/.casual/gateway/state',
   transactionURI: '/casual/.casual/transaction/state',
   domainURI: '/casual/.casual/domain/state',
   scaleURI: '/casual/.casual/domain/scale/instances',
   WEB_VERSION: 'CASUAL_WEB_VERSION'
});

app.filter('nanoToSeconds', function ()
{
   return function (nano)
   {
      let billion = 1000000000;
      return (nano / billion).toFixed(3);
   }
});

