const express = require('express');
const cors = require('cors');
const path = require('path');
const router = require('./src/api');
const bodyParser = require('body-parser');


//process.env.TZ = 'Europe/Stockholm';

let app = express();
app.use(bodyParser.json());

app.use(cors());

app.use(/^\/api/, router);



if (process.env.NODE_ENV === 'production') {
    //Static
    console.log("prod");
    app.use(express.static(path.join(__dirname, '/public/')));

    app.get(/.*/, function (req, res) {
        res.sendFile(path.join(__dirname, '/public/index.html'))
    });
}
const port = 9000;


let server = app.listen(port, function () {
    console.log("Listening on port " + port);
});

