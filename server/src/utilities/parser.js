const Category = [
    'unknown',
    'container',
    'composite',
    'integer',
    'floatingpoint',
    'character',
    'boolean',
    'string',
    'binary',
];
const jsonToYaml = function (json) {

    try {
        let service = json.service;
        let input = json.arguments.input;
        let output = json.arguments.output;

        // console.log(output);
        // console.log(input);
        let newInput = "";
        let newOutput = "";

        if (input[0]) {
            newInput = parseResult(input[0], 0);
        }
        if (output[0]) {
            newOutput = parseResult(output[0], 0);
        }

        return {
            service, input: [newInput], output: [newOutput]
        };
    } catch (error) {
        console.log(__filename);
        const line = new Error().stack.split("\n")[1];

        //console.log(line);
        console.log(error);
        throw error;
    }
};

const parseResult = function (object, level) {


    // console.log(object);
    if (!object) {
        return [];
    } else {


        let obj = {};
        // console.log(object);
        let category = Category[object.category];
        obj.category = category;
        obj.role = object.role;
        //obj[category] = object.role;
        obj["level"] = level++;
        obj["children"] = [];
        if (object.attribues.length > 0) {
            for (let child of object.attribues) {
                obj["children"].push(parseResult(child, level));
            }
        }
        return obj;
    }
};


Parser = {
    jsonToYaml
};


module.exports = Parser;