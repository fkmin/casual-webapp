const express = require('express');
const api = express.Router();
const request = require('request');

const Extract = require('./utilities/extract');
const information = require('./services/informationservice');
const admin = require('./services/updateservice');


let urls = {
    serviceURI: '/casual/.casual/service/state',
    gatewayURI: '/casual/.casual/gateway/state',
    transactionURI: '/casual/.casual/transaction/state',
    domainURI: '/casual/.casual/domain/state',
    scaleURI: '/casual/.casual/domain/scale/instances',
    WEB_VERSION: 'CASUAL_WEB_VERSION'
};


//----------------SERVER-----------------------------

//GET ALL SERVERS
api.post('/servers/all', async function (req, res) {

    let servers = await information.getAllServers();
    res.send(servers);


});

//GET SERVER BY ALIAS
api.post('/servers/server', async function (req, res) {

        try {

            let servers = await information.getAllServers();
            let server = Extract.getServerByAlias(servers, req.query.alias);

            try {

                server.services = await information.getServerServices(server.instances[0]);
            } catch (error) {
                console.error(error);
            }


            res.send(server);
        } catch (error) {
            console.error("error");
        }

    }
)
;

//SCALE SERVER
api.post('/servers/scale/server', async function (req, res) {

        try {
            let instances = req.body.instance;
            let alias = req.body.alias;

            let currentInstances = await information.getInstancesFromServer(alias);

            let newInstance = currentInstances + instances;
            if (newInstance > 0) {

                admin.scaleServer(alias, newInstance, function (response) {

                    res.send(response);
                })
            }
        } catch
            (error) {
            console.error(error);
        }


    }
)


//----------------EXECUTABLES-----------------------------

//GET ALL EXECUTABLES
api.post('/executables/all', function (req, res) {

    information.getAllExecutables(function (servers) {
        res.send(servers);
    })


});

//GET EXECUTABLE BY ALIAS
api.post('/executables/executable', async function (req, res) {

    information.getAllExecutables(function (executables) {
        let executable = Extract.getExecutableByAlias(executables, req.query.alias);

        res.send(executable);
    });


});


//----------------GROUP-----------------------------


//GET ALL GROUPS
api.post('/groups/all', function (req, res) {

    information.getAllGroups(function (response) {
        res.send(response);
    })


});

//GET GROUP BY NAME
api.post('/groups/group', async function (req, res) {

    information.getAllGroups(function (groups) {
        let group = Extract.getGroupByName(groups, req.query.name);
        group.dependencyNames = Extract.getGroupDependencyNames(groups, group.dependencies);

        res.send(group);
    });


});

//----------------SERVICE-----------------------------

//GET ALL SERVICES
api.post('/services/all', async function (req, res) {

    try {

        let services = await information.getAllServices();
        res.send(services);
    } catch (error) {
        res.status(500).send(error);
    }
});

//GET SERVICE BY NAME
api.post('/services/service', async function (req, res) {

    try {
        let servers = await information.getAllServers();

        let services = await information.getAllServices();
        let service = Extract.getServiceByName(servers, services, req.query.name);

        res.send(service);
    } catch (error) {
        console.error(error);
    }


});

api.post('/services/service/api', async function (req, res) {

    try {
        let api = await information.getServiceApi(req.query.name);
        res.send(api);
    } catch (error) {
        console.log("api-error");
        console.log(error);
    }

});

api.post('/services/service/api/available', async function (req, res) {

    try {
        let hasApi = await information.serviceApiAvailable(req.query.name);
        res.send({'api': hasApi});
    } catch (error) {
        console.log("api-error");
        console.log(error);
    }

});

//----------------GATEWAY-----------------------------
api.post('/gateways/all', async function (req, res) {

    try {


        let gateways = await information.getAllGateways();
        //let service = Parser.getServiceByName(servers, services, req.query.name);

        res.send(gateways);
    } catch (error) {
        console.error(error);
    }


});

//----------------TRANSACTION-----------------------------
api.post('/transactions/all', async function (req, res) {

    try {


        let transactions = await information.getAllTransactions();
        //let service = Parser.getServiceByName(servers, services, req.query.name);

        res.send(transactions);
    } catch (error) {
        console.error(error);
    }


});

//---------------------------------------------
api.get('/health', function (req, res) {


    res.send("Healthy");
});


module.exports = api;