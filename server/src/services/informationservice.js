const request = require('request');
const Extract = require('../utilities/extract');
const Parser = require('../utilities/parser');

const urls = {
    serviceURI: '/casual/.casual/service/state',
    gatewayURI: '/casual/.casual/gateway/state',
    transactionURI: '/casual/.casual/transaction/state',
    domainURI: '/casual/.casual/domain/state',
    scaleURI: '/casual/.casual/domain/scale/instances',
    WEB_VERSION: 'CASUAL_WEB_VERSION'
};


const getAllGroups = function (callback) {


    let options = {
        uri: 'http://localhost:8080' + urls.domainURI,
        method: 'POST',
        json: {}
    };

    request(options, function (error, response, body) {

        try {

            let responseBody = Extract.allGroups(body.result);
            //let responseBody = body.result.groups;

            //  console.log(JSON.stringify(body, null, 2));
            callback(responseBody);
        } catch (error) {

        }
    });
}

const getAllServers = function (callback) {
    let options = {
        uri: 'http://localhost:8080' + urls.domainURI,
        method: 'POST',
        json: {}
    };

    let url = 'http://localhost:8080' + urls.domainURI;
    return new Promise(function (resolve, reject) {

        request(options, function (error, response, body) {

            let responseBody = Extract.allServers(body.result);
            //      callback(responseBody);
            resolve(responseBody);
        });
    })
}

const getAllExecutables = function (callback) {
    let options = {
        uri: 'http://localhost:8080' + urls.domainURI,
        method: 'POST',
        json: {}
    };

    let url = 'http://localhost:8080' + urls.domainURI;
    request(options, function (error, response, body) {

        let responseBody = Extract.allExecutables(body.result);
        //let responseBody = body.result.executables;
        callback(responseBody);
    });
}

const getAllServices = function () {
    let options = {
        uri: 'http://localhost:8080' + urls.serviceURI,
        method: 'POST',
        json: {}
    };

    // console.log(options);

    return new Promise(function (resolve, reject) {

        request(options, function (error, response, body) {



            let responseBody = Extract.getAllServiceAverages(body.result.services);

            resolve(responseBody);
        });

    })
}
const getServiceApi = function (serviceName) {
    let options = {
        uri: 'http://localhost:8080/casual/' + serviceName,
        headers: {
            'casual-service-describe': true
        },
        method: 'POST',
        json: {}
    };


    // console.log(options);

    return new Promise(function (resolve, reject) {

        let responseBody = {
            "service": "none", input: [], output: []
        };

        request(options, function (error, response, body) {
            try {
                if (body.hasOwnProperty("model")) {
                    responseBody = Parser.jsonToYaml(body.model);
                }
                resolve(responseBody);
            } catch (error) {
                reject(responseBody);
            }
        }, function () {
            reject(responseBody);
        });

    })
}
const serviceApiAvailable = function (serviceName) {
    let options = {
        uri: 'http://localhost:8080/casual/' + serviceName,
        headers: {
            'casual-service-describe': true
        },
        method: 'POST',
        json: {}
    };
    return new Promise(function (resolve, reject) {


        let hasApi = false;

        request(options, function (error, response, body) {
            try {
                if (body.hasOwnProperty("model")) {
                    hasApi = true;
                }
                resolve(hasApi);
            } catch (error) {
                reject(hasApi);
            }
        }, function () {
            reject(hasApi);
        });
    })
};

const getServerServices = function (instancepid) {

    return new Promise(async function (resolve, reject) {

        let servicelist = [];
        let services = await getAllServices();
        for (let service of services) {


            for (let instance of service.instances.sequential) {

                if (instance.pid === instancepid.handle.pid) {
                    servicelist.push(service.name);
                }
            }
        }
        resolve(servicelist);
    });
}

const getInstancesFromServer = function (alias) {
    return new Promise(async function (resolve, reject) {

        let servers = await getAllServers();
        //console.log(JSON.stringify(servers, null,1));

        // console.log(alias);
        let server = Extract.getServerByAlias(servers, alias);


        //console.log(JSON.stringify(server, null,1));
        //   console.log(server.instances.length);
        resolve(server.instances.length);

    })
}


const getAllGateways = function () {
    let options = {
        uri: 'http://localhost:8080' + urls.gatewayURI,
        method: 'POST',
        json: {}
    };

    // console.log(options);

    return new Promise(function (resolve, reject) {

        request(options, function (error, response, body) {
            resolve(body.result);
        });

    })
}
const getAllTransactions = function () {
    let options = {
        uri: 'http://localhost:8080' + urls.transactionURI,
        method: 'POST',
        json: {}
    };

    // console.log(options);

    return new Promise(function (resolve, reject) {

        request(options, function (error, response, body) {
            resolve(body.result);
        });

    })
}


exports.getAllGroups = getAllGroups;
exports.getAllServers = getAllServers;
exports.getServerServices = getServerServices;
exports.getAllServices = getAllServices;
exports.getServiceApi = getServiceApi;
exports.serviceApiAvailable = serviceApiAvailable;
exports.getAllExecutables = getAllExecutables;
exports.getInstancesFromServer = getInstancesFromServer;
exports.getAllGateways = getAllGateways;
exports.getAllTransactions = getAllTransactions;
