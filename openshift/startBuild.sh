#!/usr/bin/env bash

dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" > /dev/null 2>&1 && pwd)"

if [[ -z "$1" ]]; then
    echo "please provide build name"
else
    oc start-build --from-archive="$dir/archive/casual-web.zip" $1
fi
