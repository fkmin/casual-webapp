#!/usr/bin/env bash

dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" > /dev/null 2>&1 && pwd)"

if [[ -z "$1" ]]; then
    echo "Please provide a name"
else
    oc new-app . --strategy=docker --name=$1
    oc expose svc/$1 --port=8080
    sleep 1
    oc start-build --from-archive="$dir/archive/casual-app.zip" $1
fi