//let domain = require('./mockAPI/backend/domain.json');


module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        config: {
            app_name: 'casual-web'

        },
        clean: ['build', 'docker', '.tmp', 'openshift/archive'],

        'string-replace': {
            dist: {
                files: [{
                    './': 'server/public/css/*.css'
                }],
                options: {
                    replacements: [{
                        pattern: '{{ VERSION }}',
                        replacement: `v<%= pkg.version %>-${Math.round(new Date().getTime() / 1000)}`
                    }]
                }
            }
        },
        exec: {
            client: {
                command: 'cd client && npm run build'
            },
            devel: {

                command: 'npm run devel'
            }
        }

    });

    grunt.loadNpmTasks('grunt-string-replace');

    grunt.loadNpmTasks('grunt-exec');


    grunt.registerTask('build-client', ['exec:client']);

    grunt.registerTask('replace', ['build', 'string-replace']);

    grunt.registerTask('build', ['build-client', 'string-replace']);
    grunt.registerTask('devel', ['exec:devel']);


};


function getJson(json) {
    let mockServer = require('./mockAPI/server');
    return mockServer(json);
}
