# casual-webapp


### Setup development environment 

Make sure you are in root directory of project

#### Install
> Install nodejs   
> Install dependencies with `npm install`  

#### Start webapp with grunt
> `grunt serve`    
> In browser: `localhost:8000`

#### Start webapp without uglify/minified files  
> `grunt dev`  
> In browser: `localhost:8000/app`  


### Build for openshift  
> `grunt openshift`    
> `openshift/newApp.sh <name>`  
#### If app and build exists
>  `openshift/startBuild.sh <name>`


##Changelog  
   

####0.3.1 (2019-05-19)
    
* Web GUI get an update
* Web GUI get an update
* Particle information on a Group and Server are now visible
    
####0.3.0 (2019-05-15)
* All features from previous builds are not yet implemented
* Web application is now built with Nodejs and Vue
* First simple list view of Groups, Servers and Services implemented
* Pages for particle information prepared</li>
