const axios = require('axios');

export const getAllServers = function (callback) {
    axios.post('/api/servers/all').then(function (res) {
        //   console.log(res);
        callback(res.data);

    }).catch(function (error) {
        console.log(error);
    })
}

export const getServerByName = function (serverAlias, callback) {
    axios.post('/api/servers/server', null, {params: {alias: serverAlias}}).then(function (res) {
        //console.log(res);
        callback(res.data);

    }).catch(function (error) {
        console.log(error);
    })
}

export const scaleInstance = function (alias, instance, callback) {

    axios.post('/api/servers/scale/server', {alias: alias, instance: instance}).then(function (res) {
        callback(res.data);
    });
}

export const getAllGroups = function (callback) {
    axios.post('/api/groups/all').then(function (res) {
        // console.log(res);
        callback(res.data);

    }).catch(function (error) {
        console.log(error);
    })
}

export const getGroupByName = function (groupName, callback) {
    axios.post('/api/groups/group', null, {params: {name: groupName}}).then(function (res) {
        // console.log(res);
        callback(res.data);

    }).catch(function (error) {
        console.log(error);
    })
}

export const getAllServices = function (callback) {
    axios.post('/api/services/all').then(function (res) {
        //console.log(JSON.stringify(res, null,1));


        callback(res.data);

    }).catch(function (error) {
        console.log(error);
    })
}

export const getServiceByName = function (serviceName, callback) {
    console.log(serviceName);
    axios.post('/api/services/service', null, {params: {name: serviceName}}).then(function (res) {
        callback(res.data);

    }).catch(function (error) {
        console.log(error);
    })
}

export const getServiceApi = function (serviceName, callback) {
    console.log(serviceName);

    let config = {
        params: {
            name: serviceName
        }
    }

    axios.post('/api/services/service/api', null, {params: {name: serviceName}}).then(function (res) {
        // console.log(JSON.stringify(res.data, null, 1));
        callback(res.data);

    }).catch(function (error) {
        console.log(error);
    })
}

export const serviceApiAvailable = function (serviceName) {
    return new Promise(function (resolve, reject) {

        axios.post('/api/services/service/api/available', null, {params: {name: serviceName}}).then(function (res) {
            resolve(res.data);

        }).catch(function (error) {
            console.log(error);
        })
    })
};


export const getAllExecutables = function (callback) {

    axios.post('/api/executables/all').then(function (response) {
        callback(response.data);
    }).catch(function (error) {
        console.log(error);
    })

}

export const getExecutableByAlias = function (executableAlias, callback) {
    console.log(executableAlias);
    axios.post('/api/executables/executable', null, {params: {alias: executableAlias}}).then(function (res) {
        callback(res.data);

    }).catch(function (error) {
        console.log(error);
    })
}

export const getAllGateways = function (callback) {

    axios.post('/api/gateways/all').then(function (response) {
        callback(response.data);
    }).catch(function (error) {
        console.log(error);
    })

}

export const getAllTransactions = function (callback) {

    axios.post('/api/transactions/all').then(function (response) {
        callback(response.data);
    }).catch(function (error) {
        console.log(error);
    })

}




