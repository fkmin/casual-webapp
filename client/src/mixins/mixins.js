import {
    getAllServers,
    getServerByName,
    getAllGroups,
    getGroupByName,
    getAllServices,
    getServiceByName,
    getServiceApi,
    serviceApiAvailable,
    getAllExecutables,
    getExecutableByAlias,
    scaleInstance,
    getAllGateways,
    getAllTransactions
} from '../services/BackendService';


export const mixin = {
        data() {
            return {
                yaml: [],
                refreshButtonText : 'Refresh',
            }
        },
        methods: {
            getAllServers,
            getServerByName,
            getAllGroups,
            getGroupByName,
            getAllServices,
            getServiceByName,
            getServiceApi,
            serviceApiAvailable,
            getAllExecutables,
            getExecutableByAlias,
            scaleInstance,
            getAllGateways,
            getAllTransactions,
            formatDate: function (nano) {


                let milli = this.toMilli(nano);
                let date = "-";
                try {
                    let last = milli;
                    if (last !== "" && last > 0) {
                        let timeZoneOffset = (new Date()).getTimezoneOffset() * 60000;
                        let newDate = new Date(last - timeZoneOffset).toISOString().split(/[A-Z]/);
                        date = newDate[0] + " | " + newDate[1];
                    }
                } catch (erro) {
                    console.log(erro);
                }
                return date;
            },
            toMilli: function (nano) {
                let million = 1000000;
                return nano / million;
            },
            parseYamlString: function (obj) {

                const role = obj.role !== "" ? obj.role : "-";
                const category = obj.category;
                const level = obj.level;


                this.yaml.push({
                    category: category,
                    role: role,
                    level: level
                });

                if (obj.children.length !== 0) {
                    for (let child of obj.children) {
                        this.yaml.push(this.parseYamlString(child));
                    }
                } else {

                }

            },

            toYaml: function (json) {

                this.yaml = [];
                if (json[0]) {
                    this.parseYamlString(json[0]);
                }

                 //console.log(JSON.stringify(this.yaml, null, 1));
                // console.log(JSON.stringify(json, null, 1));
                return this.yaml;
            }
        }
    }
;

