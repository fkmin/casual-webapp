import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        user: {
            name: ''
        },
        nameTitle: ''
    },
    getters: {
        loggedInUser: function (state) {
            return state.user;
        },
        getTitle: function (state) {
            console.log(state.nameTitle);
            return state.nameTitle;
        }
    },
    mutations: {
        logIn: function (state, username) {
            state.user.name = username;
        },
        formatDate: function (state, date) {
            console.log(date);
            return "hello";
        },
        setTitleName: function (state, name) {
            console.log(name);

            state.nameTitle = name;
        }
    }
})
