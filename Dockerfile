FROM casual/middleware

USER root

RUN curl -sL https://rpm.nodesource.com/setup_8.x | bash - \
 && yum install -y nodejs


EXPOSE 9000

ENV CASUAL_DOMAIN_HOME=/test/casual \
    CASUAL_FIELD_TABLE=/test/casual/fields/field.yaml


COPY ./openshift/domainA.yaml /test/casual/configuration/
COPY ./openshift/domain.yaml /test/casual/configuration/
COPY ./openshift/field.yaml /test/casual/fields/


RUN chown casual /test/casual/configuration/domain.yaml && \
. $CASUAL_HOME/etc/bash_completion.d/casual

COPY ./server/ /usr/casual/webapp/
WORKDIR /usr/casual/webapp

RUN npm install --production
ENV NODE_ENV production

WORKDIR $CASUAL_DOMAIN_HOME



USER casual
